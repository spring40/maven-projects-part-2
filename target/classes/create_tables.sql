CREATE SCHEMA EDU;

DROP table EDU.EMPLOYEE;
CREATE table EDU.EMPLOYEE (
	ID INTEGER NOT NULL PRIMARY KEY,
	DEPT_ID INTEGER NOT NULL,
	NAME VARCHAR(50),
	SALARY INTEGER NOT NULL
	
);
DROP TABLE EDU.DEPARTMENT;
CREATE table EDU.DEPARTMENT (
	DEPT_ID INTEGER NOT NULL PRIMARY KEY,
	DEPT_NAME VARCHAR(30)
);
INSERT INTO EDU.DEPARTMENT VALUES (1,'Finance');
INSERT INTO EDU.DEPARTMENT VALUES (2,'IT');
INSERT INTO EDU.EMPLOYEE VALUES (100,1,'Ivanov Ivan',1000);
INSERT INTO EDU.EMPLOYEE VALUES (101,1,'Petrov Petr',2000);
INSERT INTO EDU.EMPLOYEE VALUES (102,2,'Sidorov Alex',1500);